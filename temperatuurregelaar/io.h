#ifndef IO_H_
#define IO_H_

// input/output support functions for temperatuurregelaar

#include <stdbool.h>
#include <stdint.h>

// tick variable is set to true every sample time.
extern volatile bool tick;

bool initUART(void);
bool initTimer(uint32_t sampleFrequency);
bool initADC(void);
bool initPWM(void);

// readADC reads temperature in Q13.2 fixed-point format.
// returns INT16_MIN on error.
int16_t readADC(void);
// setPWM sets dutyCycle in 1000 steps (min = 0, max = 1000)
bool setPWM(uint16_t dutyCycle);

void sendStringUART(char *s);
void sendIntUART(int i);
void sendIntIntUART(int i1, int i2);

#endif
