// input/output support functions for temperatuurregelaar

#include "io.h"
#include "Board.h"

// Driver header files
// #include <ti/drivers/GPIO.h>
#include <ti/display/Display.h>
#include <ti/drivers/Timer.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/PWM.h>

// tick variable is set to true every sample time.
volatile bool tick = false;

// Defines for MCP9808 temperature sensor
#define MCP9808addr 0x18
#define CONF_REG 0x01
#define RES_REG 0x08
#define AMB_TEMP_REG 0x05

static Display_Handle display;

bool initUART(void) {
    Display_init();
    // Open the HOST display for output via UART
    display = Display_open(Display_Type_UART, NULL);
    return display != NULL;
}

static void timerTick(Timer_Handle handle)
{
    tick = true;
}

bool initTimer(uint32_t sampleFrequency)
{
    Timer_init();

    Timer_Handle handle;
    Timer_Params params;
    Timer_Params_init(&params);
    params.periodUnits = Timer_PERIOD_HZ;
    params.period = sampleFrequency;
    params.timerMode  = Timer_CONTINUOUS_CALLBACK;
    params.timerCallback = timerTick;
    handle = Timer_open(Board_TIMER0, &params);
    if (handle == NULL)
    {
        return false;
    }
    int32_t status = Timer_start(handle);
    if (status == Timer_STATUS_ERROR)
    {
        return false;
    }
    return true;
}

static I2C_Handle i2c;

bool initADC(void)
{
    uint8_t txBuffer[3];
    uint8_t rxBuffer[2];
    I2C_Params i2cParams;
    I2C_Transaction i2cTransaction;

    I2C_init();

    /* Create I2C for usage */
    I2C_Params_init(&i2cParams);
    i2cParams.bitRate = I2C_100kHz;
    i2c = I2C_open(Board_I2C_TMP, &i2cParams);
    if (i2c == NULL)
    {
        return false;
    }

    // Setup configuration register
    // Not needed because All power-up default
    // http://ww1.microchip.com/downloads/en/DeviceDoc/25095A.pdf#page=18
    txBuffer[0] = CONF_REG;
    txBuffer[1] = 0;
    txBuffer[2] = 0x02; // Alert Output Polarity is Active-high (Not needed because Alert is not used.)
    i2cTransaction.slaveAddress = MCP9808addr;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.writeCount = 3;
    i2cTransaction.readBuf = rxBuffer;
    i2cTransaction.readCount = 0;
    if (I2C_transfer(i2c, &i2cTransaction) == false)
    {
        return false;
    }

    // Setup resolution register
    // http://ww1.microchip.com/downloads/en/DeviceDoc/25095A.pdf#page=28
    txBuffer[0] = RES_REG;
    txBuffer[1] = 0x01; // resolution = 0.25�C (tCONV = 65 ms typical)
    i2cTransaction.slaveAddress = MCP9808addr;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.writeCount = 2;
    i2cTransaction.readBuf = rxBuffer;
    i2cTransaction.readCount = 0;
    if (I2C_transfer(i2c, &i2cTransaction) == false)
    {
        return false;
    }
    return true;
}

static PWM_Handle pwm0;

bool initPWM(void)
{
    PWM_Params pwmParams;

    PWM_init();

    // PWM params init
    PWM_Params_init(&pwmParams);
    pwmParams.dutyUnits = PWM_DUTY_US;
    // PWM duty cycle = 0
    pwmParams.dutyValue = 0;
    pwmParams.periodUnits = PWM_PERIOD_US;
    // PWM frequency = 1 / 1000 us = 1 / 1 ms = 1 kHz.
    pwmParams.periodValue = 1000;
    pwmParams.idleLevel = PWM_IDLE_LOW;

    /* Open PWM0 */
    pwm0 = PWM_open(Board_PWM2, &pwmParams);

    if (!pwm0)
    {
        return false;
    }
    PWM_start(pwm0);
    return true;
}

int16_t readADC(void)
{
    uint8_t txBuffer[3];
    uint8_t rxBuffer[2];
    I2C_Transaction i2cTransaction;

    // Read ambient temperature
    // http://ww1.microchip.com/downloads/en/DeviceDoc/25095A.pdf#page=24
    txBuffer[0] = AMB_TEMP_REG;
    i2cTransaction.slaveAddress = MCP9808addr;
    i2cTransaction.writeBuf = txBuffer;
    i2cTransaction.writeCount = 1;
    i2cTransaction.readBuf = rxBuffer;
    i2cTransaction.readCount = 2;
    I2C_transfer(i2c, &i2cTransaction);
    if (I2C_transfer(i2c, &i2cTransaction) == false)
    {
        return INT16_MIN;
    }
    // get temperature measurement
    if (rxBuffer[0] & 0x10)
    {
        // temperature is negative.
        rxBuffer[0] |= 0xF0;
    }
    else
    {
        // temperature is positive.
        rxBuffer[0] &= 0x1F;
    }
    return ((((int16_t)(rxBuffer[0])<<8) + rxBuffer[1])>>2);
}

bool setPWM(uint16_t dutyCycle)
{
    if (PWM_setDuty(pwm0, dutyCycle) < 0)
    {
        return false;
    }
    return true;
}

void sendStringUART(char *s)
{
    Display_printf(display, 0, 0, s);
}

void sendIntUART(int i)
{
    Display_printf(display, 0, 0, "%d", i);
}

void sendIntIntUART(int i1, int i2)
{
    Display_printf(display, 0, 0, "%d, %d", i1, i2);
}
