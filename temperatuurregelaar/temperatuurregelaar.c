#include "io.h"

uint16_t    setpoint=(49<<2);
int         i=0;
int16_t     e=0;
int32_t     u=0;
int16_t     buffer[3];
int16_t     t=0;
int32_t     a=0;

void *mainThread(void *arg0)
{
    if (initUART() == false)
    {
        // initUart() failed
        while(1);
    }
    // Samplefrequency = 10 Hz
    if (initTimer(10) == false)
    {
        sendStringUART("initTimer() failed");
        while(1);
    }
    if (initADC() == false)
    {
        sendStringUART("initADC() failed");
        while(1);
    }
    if (initPWM() == false)
    {
        sendStringUART("initPWM() failed");
        while(1);
    }
    int ticks = 0; // counter for ticks
    uint16_t dutyCycle = 0;
    while(1)
    {
        // generate step function after 50 ticks
        if (ticks < 50)
        {
            ticks = ticks + 1;
        }
        else
        {
            a = u;
            if(u<0)
            {
                a=0;
            }
            if(u>1000)
            {
                a=1000;
            }
            dutyCycle = a;
        }

        setPWM(dutyCycle);

        if (t == INT16_MIN)
        {
            sendStringUART("readADC failed!");
        }
        else
        {
            sendIntIntUART(dutyCycle / 10, t);
        }

        t = readADC();

        for(i=2;i>0;i--)
        {
            buffer[i]=buffer[i-1];
        }

        e = setpoint-t;

        buffer[0] = e;

        u = u + 234.62*buffer[0]-443.18*buffer[1]+209.28*buffer[2];  //Ziegler Nichols (PID)  u = u + 4991*buffer[0]-9950*buffer[1]+4960*buffer[2];//Cohen Coon (PID)        u = u + 174*buffer[0]-321*buffer[1]+148*buffer[2];//ITAE (PID)

        if (tick == true)
        {
            sendStringUART("The calculation is not fast enough");
        }
        while (tick == false)
        {
            // wait for next timer tick
        }
        tick = false;
    }
}
